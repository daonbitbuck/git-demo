package com.nttdata.demo.gitdemo.comman;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.demo.gitdemo.model.EsitoBean;

public class Test {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        EsitoBean esitoBean = new EsitoBean();
        esitoBean.setCode("200");
        esitoBean.setMessage("Success");
        System.out.println(mapper.writeValueAsString(esitoBean));
        System.out.println("Hello from master");
    }
}
